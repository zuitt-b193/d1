package com.zuitt.batch193;

import java.util.Scanner;

public class UserInput {

    public static void main(String[] args) {
        //Scanner class = to add/get user input; imported from java util package
        //Scanner is an object
        Scanner appScanner = new Scanner(System.in);

        System.out.println("What's your name?");
        //nextLine converts value into an object
        //.trim removing white spaces before and after the words
        String myName = appScanner.nextLine().trim();
        System.out.println("Username is: " + myName);

        System.out.println("What's your age?");
        int myAge = appScanner.nextInt();
        System.out.println("Age of the user is: " + myAge);

        System.out.println("What's your weight?");
        double myWeight = appScanner.nextDouble();
        System.out.println("Weight of user is: " + myWeight + "kgs");

    }

}
