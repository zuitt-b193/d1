package com.zuitt.batch193;

public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello World");
    }
    //public = access modifier
    //static (Non access modifier) = how should this method behave
    //static keyword field exists across all the class instances
    //void = return type
    //main = method name


}
