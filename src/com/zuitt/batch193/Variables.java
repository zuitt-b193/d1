package com.zuitt.batch193;

import java.util.Locale;

public class Variables {

    public static void main(String[] args) {
        //variable declaration
        int myNum;

        //variable initialization
        myNum = 29; //Literals are any constant value each can be assigned to the variable

        System.out.println(myNum);

        //Reassignment
        myNum = 1;
        System.out.println(myNum);

        int age;
        char middle_name;

        //constant value (all caps in name)
        final int PRINCIPAL = 1000;
        final String LAST_NAME = "DOE";

        //Primitive data types
        //single quote to specify a single char
        char letter = 'A';
        boolean isMArried = false;
        byte students = 127;
        int localPopulation = 2_146_273_827; //underscore not read as value

        System.out.println(localPopulation);

        long worldPopulation = 2_146_273_827L; //L meaning long literal

        //Decimal points
        float price = 12.99F; //F = float
        double temperature = 1345.43254;

        //getClass() = grab data type of variable
        System.out.println(((Object)temperature).getClass());

        //non primitive data types / referencing data types
        //String
        String name = "John Doe";
        System.out.println(name);
        //String can access methods to manipulate data
        String editedName = name.toLowerCase();
        System.out.println(editedName);
        System.out.println(name.getClass());

        //escape character
        System.out.println("c:\\windows\\desktop"); //   \", \n

        //explicit casting
        int num1 = 5;
        double num2 = 2.7;
        double total = (double) (num1 + num2);
        System.out.println(total);

        //both strings (will be concatenated)
        String mathGrade = "90";
        String englishGrade = "85";
        System.out.println(mathGrade + englishGrade);
        //convert Strings to int
        int totalGrade = Integer.parseInt(mathGrade) + Integer.parseInt(englishGrade);
        System.out.println(totalGrade);
        //convert int to String
        String stringGrade = Integer.toString(totalGrade);
        System.out.println(stringGrade.getClass());


    }
}
